package com.packages.coffeemachine;

import com.packages.coffeemachine.feature.CoffeeMachine;

public class Main {
    public static void main(String[] args) {
        CoffeeMachine cmObj = new CoffeeMachine();
        cmObj.readUserActions();
        cmObj.processUserActions();
    }
}
