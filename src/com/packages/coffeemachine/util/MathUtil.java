package com.packages.coffeemachine.util;

public final class MathUtil {

    public static int findSmallestNumber(int[] numbers, int numberOfIngredients) {
        int minValue = numbers[0];
        for (int i = 0; i < numberOfIngredients; i++) {
            if (numbers[i] < minValue) {
                minValue = numbers[i];
            }
        }
        return minValue;
    }








}
