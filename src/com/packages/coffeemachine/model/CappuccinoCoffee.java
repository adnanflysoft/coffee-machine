package com.packages.coffeemachine.model;

public class CappuccinoCoffee extends CoffeeTypes {

    public CappuccinoCoffee() {
        super(200, 100,15, 30, 1, 6);
    }
}
