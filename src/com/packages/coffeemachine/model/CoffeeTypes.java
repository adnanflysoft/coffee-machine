package com.packages.coffeemachine.model;

public class CoffeeTypes {

    private final int waterRequired;
    private final int milkRequired;
    private final int beansRequired;
    private final int sugarRequired;
    private final int cupsRequired;
    private final int moneyPerCoffee;

    CoffeeTypes(int waterRequired, int milkRequired, int beansRequired, int sugarRequired, int cupsRequired, int moneyPerCoffee) {
        this.waterRequired = waterRequired;
        this.milkRequired = milkRequired;
        this.beansRequired = beansRequired;
        this.sugarRequired = sugarRequired;
        this.cupsRequired = cupsRequired;
        this.moneyPerCoffee = moneyPerCoffee;
    }

    public int getWaterRequired() {
        return waterRequired;
    }

    public int getMilkRequired() {
        return milkRequired;
    }

    public int getBeansRequired() {
        return beansRequired;
    }

    public int getSugarRequired() {
        return sugarRequired;
    }

    public int getCupsRequired() {
        return cupsRequired;
    }

    public int getMoneyPerCoffee() {
        return moneyPerCoffee;
    }


}
