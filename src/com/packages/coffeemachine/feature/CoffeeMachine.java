package com.packages.coffeemachine.feature;

import com.packages.coffeemachine.feature.ingredients.IngredientsDisplayer;
import com.packages.coffeemachine.feature.ingredients.IngredientsUpdator;
import com.packages.coffeemachine.feature.money.MoneyCollector;

import java.util.Scanner;

public class CoffeeMachine {

    private int userAction;
    private int water;
    private int milk;
    private int coffeeBeans;
    private int sugar;
    private int disposableCups;
    private int totalCollection;

    public void readUserActions() {
        Scanner readObj = new Scanner(System.in);
        System.out.println("\n\nChoose Action \n 1. Buy Coffee\t2. Fill Ingredients\t3. Take Money\t4. Check Ingredients Available\t5. Stop");
        this.userAction = readObj.nextInt();
    }

    public void processUserActions() {
        switch (this.userAction) {
            case 1 :    CoffeeMaker coffeeMakerObj = new CoffeeMaker();
                        coffeeMakerObj.readRequiredCoffees(this);
                        break;

            case 2 :    IngredientsUpdator ingObj = new IngredientsUpdator();
                        ingObj.fillIngredientsService(this);
                        break;

            case 3 :    MoneyCollector mnObj = new MoneyCollector();
                        mnObj.collectMoneyService(this);
                        break;

            case 4 :    IngredientsDisplayer dspObj = new IngredientsDisplayer();
                        dspObj.displayIngredientsAvailable(this);
                        break;

            case 5 :    break;

            default :   System.out.println("Invalid Input");
                        break;
        }

        if(this.userAction != 5) {
            this.readUserActions();
            this.processUserActions();
        }
    }

    public int getWater() {
        return water;
    }

    public int getMilk() {
        return this.milk;
    }

    public int getCoffeeBeans() {
        return this.coffeeBeans;
    }

    public int getSugar() {
        return this.sugar;
    }

    public int getDisposableCups() {
        return this.disposableCups;
    }

    public int getTotalCollection() {
        return this.totalCollection;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }

    public void setCoffeeBeans(int beans) {
        this.coffeeBeans = beans;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public void setDisposableCups(int cups) {
        this.disposableCups =cups;
    }

    public void setTotalCollection(int collection) {
        this.totalCollection = collection;
    }
}
