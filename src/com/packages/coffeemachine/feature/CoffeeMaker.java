package com.packages.coffeemachine.feature;

import com.packages.coffeemachine.feature.ingredients.IngredientsChecker;
import com.packages.coffeemachine.feature.ingredients.IngredientsUpdator;
import com.packages.coffeemachine.feature.money.MoneyCalculator;
import com.packages.coffeemachine.model.CappuccinoCoffee;
import com.packages.coffeemachine.model.CoffeeTypes;
import com.packages.coffeemachine.model.EspressoCoffee;
import com.packages.coffeemachine.model.LatteCoffee;

import java.util.Scanner;

public class CoffeeMaker {

    private int coffeesRequired;
    private int coffeeType;

    public void readRequiredCoffees(CoffeeMachine coffeeMachineObj) {
        Scanner readObj = new Scanner(System.in);
        System.out.println("Enter Coffee Type \n1. Espresso, 2. Latte, 3. Cappuccino:");
        this.coffeeType = readObj.nextInt();
        System.out.println("Enter number of coffees required");
        this.coffeesRequired = readObj.nextInt();
        IngredientsChecker ingredientsCheckerObj = new IngredientsChecker();
        MoneyCalculator moneyCalculatorObj = new MoneyCalculator();
        IngredientsUpdator ingredientsUpdatorObj = new IngredientsUpdator();
        processCoffeeTypes(ingredientsCheckerObj, moneyCalculatorObj, ingredientsUpdatorObj, coffeeMachineObj);
    }

    private void processCoffeeTypes(IngredientsChecker ingredientsCheckerObj, MoneyCalculator moneyCalculatorObj, IngredientsUpdator ingredientsUpdatorObj, CoffeeMachine coffeeMachineObj) {

        boolean confirmPayment = false;
        CoffeeTypes coffeeTypeObj = null;
        switch (this.coffeeType) {
            case 1:
                coffeeTypeObj = new EspressoCoffee();
                break;
            case 2:
                coffeeTypeObj = new LatteCoffee();
                break;
            case 3:
                coffeeTypeObj = new CappuccinoCoffee();
                break;
            default:
                System.out.println("Invalid Input");
                break;
        }
        if (coffeeTypeObj != null) {
            makeCoffee(ingredientsCheckerObj, moneyCalculatorObj, ingredientsUpdatorObj, coffeeMachineObj, confirmPayment, coffeeTypeObj);
        }
    }

    private void makeCoffee(IngredientsChecker ingredientsCheckerObj, MoneyCalculator moneyCalculatorObj, IngredientsUpdator ingredientsUpdatorObj, CoffeeMachine coffeeMachineObj, boolean confirmPayment, CoffeeTypes coffeeTypeObj) {
        boolean isIngredientsAvailable;
        isIngredientsAvailable = ingredientsCheckerObj.checkIngredientsAvailable(this, coffeeTypeObj, coffeeMachineObj);
        if (isIngredientsAvailable) {
            confirmPayment = moneyCalculatorObj.calculateAndConfirmPayment(this, coffeeTypeObj, coffeeMachineObj);
        }
        if (confirmPayment) {
            this.makeAndServeCoffee();
            ingredientsUpdatorObj.clearIngredientsConsumed(coffeeMachineObj, this, coffeeTypeObj);
        }
    }

    public void makeAndServeCoffee() {
        System.out.println("Starting to make a coffee");
        System.out.println("Grinding coffee beans");
        System.out.println("Boiling water");
        System.out.println("Mixing boiled water with crushed coffee beans");
        System.out.println("Pouring coffee into the cup");
        System.out.println("Pouring some milk into the cup");
        System.out.println("Coffee is ready!\n\n\n");
    }

    public int getCoffeesRequired() {
        return coffeesRequired;
    }

    public int getCoffeeType() {
        return coffeeType;
    }

}
