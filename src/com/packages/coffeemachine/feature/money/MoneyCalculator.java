package com.packages.coffeemachine.feature.money;

import com.packages.coffeemachine.feature.CoffeeMachine;
import com.packages.coffeemachine.feature.CoffeeMaker;
import com.packages.coffeemachine.model.CoffeeTypes;

import java.util.Scanner;

public class MoneyCalculator {

    private int totalAmount;

    public boolean calculateAndConfirmPayment(CoffeeMaker coffeeMakerObj, CoffeeTypes coffeeTypeObj, CoffeeMachine coffeeMachineObj) {
        Scanner readObj = new Scanner(System.in);
        this.totalAmount = coffeeMakerObj.getCoffeesRequired() * coffeeTypeObj.getMoneyPerCoffee();
        System.out.println("Total Amount : $" + totalAmount);
        System.out.println("Confirm Payment 1. Yes 2. No");
        int confirmVariable = readObj.nextInt();
        if(confirmVariable == 1) {
            int totalCollection = coffeeMachineObj.getTotalCollection() + totalAmount;
            coffeeMachineObj.setTotalCollection(totalCollection);
            return true;
        } else {
            System.out.println("Thank You");
            return false;
        }
    }

    public int getTotalAmount() {
        return totalAmount;
    }

}
