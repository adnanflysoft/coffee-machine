package com.packages.coffeemachine.feature.money;

import com.packages.coffeemachine.feature.CoffeeMachine;

public class MoneyCollector {

    public void collectMoneyService(CoffeeMachine cmObj) {
        System.out.println("Available Money : " + cmObj.getTotalCollection());
        System.out.println("Collecting Money...");
        cmObj.setTotalCollection(0);
    }

}
