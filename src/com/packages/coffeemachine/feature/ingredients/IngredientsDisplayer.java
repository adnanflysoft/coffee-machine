package com.packages.coffeemachine.feature.ingredients;

import com.packages.coffeemachine.feature.CoffeeMachine;

public class IngredientsDisplayer {

    public void displayIngredientsAvailable(CoffeeMachine cmObj) {
        System.out.println("The Coffee Machine has : ");
        System.out.println(cmObj.getWater() + " ml of Water");
        System.out.println(cmObj.getMilk() + " ml of Milk");
        System.out.println(cmObj.getCoffeeBeans() + " grams of Coffee Beans");
        System.out.println(cmObj.getSugar() + " grams of Sugar");
        System.out.println(cmObj.getDisposableCups() + " no of of Disposable Cups");
        System.out.println("$" + cmObj.getTotalCollection() + " Money Available");
    }

}
