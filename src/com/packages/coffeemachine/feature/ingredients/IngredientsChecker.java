package com.packages.coffeemachine.feature.ingredients;

import com.packages.coffeemachine.feature.CoffeeMachine;
import com.packages.coffeemachine.feature.CoffeeMaker;
import com.packages.coffeemachine.util.MathUtil;
import com.packages.coffeemachine.model.CoffeeTypes;

public class IngredientsChecker {

    public boolean checkIngredientsAvailable(CoffeeMaker coffeeMakerObj, CoffeeTypes coffeeTypeObj, CoffeeMachine coffeeMachineObj) {
        int numberOfIngredients;
        int[] numbers;
        if(coffeeMakerObj.getCoffeeType() != 1) {
            numberOfIngredients = 5;
            numbers = new int[numberOfIngredients];
            numbers[4] = coffeeMachineObj.getMilk() / coffeeTypeObj.getMilkRequired();
        } else {
            numberOfIngredients = 4;
            numbers = new int[numberOfIngredients];
        }
//        int[] numbers = new int[numberOfIngredients];
        numbers[0] = coffeeMachineObj.getWater() / coffeeTypeObj.getWaterRequired();
        numbers[1] = coffeeMachineObj.getCoffeeBeans() / coffeeTypeObj.getBeansRequired();
        numbers[2] = coffeeMachineObj.getSugar() / coffeeTypeObj.getSugarRequired();
        numbers[3] = coffeeMachineObj.getDisposableCups() / coffeeTypeObj.getCupsRequired();
        int minNum = MathUtil.findSmallestNumber(numbers, numberOfIngredients);
        if(coffeeMakerObj.getCoffeesRequired() <= minNum) {
            System.out.println("Yes, I can make " + coffeeMakerObj.getCoffeesRequired() + " Coffees.");
            return true;
        } else {
            System.out.println("Sorry, I can make " + minNum + " Coffees only.");
            return false;
        }
    }

}
