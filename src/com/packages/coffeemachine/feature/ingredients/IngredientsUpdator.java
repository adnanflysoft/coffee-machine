package com.packages.coffeemachine.feature.ingredients;

import com.packages.coffeemachine.feature.CoffeeMachine;
import com.packages.coffeemachine.feature.CoffeeMaker;
import com.packages.coffeemachine.model.CoffeeTypes;

import java.util.Scanner;

public class IngredientsUpdator {

    public void fillIngredientsService(CoffeeMachine cmObj) {
        Scanner readObj = new Scanner(System.in);
        System.out.println("You are going to fill resources, \nEnter the amount of Water (in ml)\t");
        int water = cmObj.getWater() + readObj.nextInt();
        cmObj.setWater(water);
        System.out.println("Enter the amount of Milk (in ml)\t");
        int milk = cmObj.getMilk() + readObj.nextInt();
        cmObj.setMilk(milk);
        System.out.println("Enter the amount of Coffee Beans (in grams)\t");
        int beans = cmObj.getCoffeeBeans() + readObj.nextInt();
        cmObj.setCoffeeBeans(beans);
        System.out.println("Enter the amount of Sugar (in grams)\t");
        int sugar = cmObj.getSugar() + readObj.nextInt();
        cmObj.setSugar(sugar);
        System.out.println("Enter the number of Disposable Cups\t");
        int cups = cmObj.getDisposableCups() + readObj.nextInt();
        cmObj.setDisposableCups(cups);
    }

    public void clearIngredientsConsumed(CoffeeMachine coffeeMachineObj, CoffeeMaker coffeeMakerObj, CoffeeTypes coffeeTypeObj) {
        int water = coffeeMachineObj.getWater() - (coffeeMakerObj.getCoffeesRequired() * coffeeTypeObj.getWaterRequired());
        coffeeMachineObj.setWater(water);
        int milk = coffeeMachineObj.getMilk() - (coffeeMakerObj.getCoffeesRequired() * coffeeTypeObj.getMilkRequired());
        coffeeMachineObj.setMilk(milk);
        int beans = coffeeMachineObj.getCoffeeBeans() - (coffeeMakerObj.getCoffeesRequired() * coffeeTypeObj.getBeansRequired());
        coffeeMachineObj.setCoffeeBeans(beans);
        int sugar = coffeeMachineObj.getSugar() - (coffeeMakerObj.getCoffeesRequired() * coffeeTypeObj.getSugarRequired());
        coffeeMachineObj.setSugar(sugar);
        int cups = coffeeMachineObj.getDisposableCups() - (coffeeMakerObj.getCoffeesRequired() * coffeeTypeObj.getCupsRequired());
        coffeeMachineObj.setDisposableCups(cups);
    }

}
